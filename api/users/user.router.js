const { createUser, getUsers, getUser, updateUser, deleteUser, login } = require('./user.controller');
const router = require('express').Router();
const { checkToken } = require('../../auth/token_validation');

router.get('/', checkToken, getUsers);
router.post('/create', createUser);
router.get('/:id', checkToken, getUser);
router.patch('/update/:id', checkToken, updateUser);
router.delete('/delete/:id', checkToken, deleteUser);
router.post('/login', login);

module.exports = router;