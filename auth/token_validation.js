require('dotenv').config();
const { verify } = require('jsonwebtoken');

module.exports = {
    checkToken: (req, res, next) => {
        let token = req.get('authorization');
        if(token) {
            // Bearer has 6 lettes and then a space so total is 7
            token = token.slice(7);
            verify(token, process.env.TOKEN_KEY, (err, decoded) => {
                if (err) {
                    res.json({
                        success: 0,
                        message: "Invalid Token"
                    });             
                } else {
                    next();
                }
            });
        } else {
            res.json({
                success: 0,
                message: "Access denied! unauthorized user"
            });
        }
    }
}